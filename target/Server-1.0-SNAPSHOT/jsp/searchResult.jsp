﻿<%@ page language="java" contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.ArrayList"%>
<html>
<head>
    <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style2.css" rel="stylesheet">
    <title><c:out value="${headers[0]}"/></title>
</head>
<body>
    <div class="menuLine"></div>
    <div class="searchMenu" id="menuBrick"></div>
    <div class="content">
        <div id="mainLayer">
           <div class="result" id="result">
                <jsp:useBean id="ob" scope="page" class="servlet.presentationLayer.View" />
                <c:forEach var="item" items="${allData}" varStatus="status">
                    <c:if test = "${type == '1'}">
                        <div class="titleName"><c:out value="${item.position}"/></div>
                        <div><b><c:out value="${content[0]}"/>
                        <c:choose>
                            <c:when test="${item.salary != 0.}">
                                <c:out value="${item.salary}"/> <c:out value="${item.salaryCurrency}"/>
                            </c:when>
                            <c:otherwise><c:out value="${content[1]}"/></c:otherwise>
                        </c:choose>
                        </b></div>
                        <div><c:out value="${item.requirements}"/></div>
                        <c:set var="employer" value="${ob.getEmployerByID(item)}"/>
                        <c:set var="company" value="${ob.getCompanyByID(employer)}"/>
                        <div><c:out value="${content[2]}"/><c:out value="${company.name}"/></div>
                        <div><c:out value="${content[3]}"/><c:out value="${item.timeAdded}"/></div>
                        <form action="Server" method="post">
                            <input type="hidden" name="action" value="moreInfo"/>
                            <input type="hidden" name="itemID" value="${item.id}"/>
                            <input type="hidden" name="type" value="vacancy"/>
                            <button onclick="sendOther('form${status.index}');"><c:out value="${content[4]}"/></button>
                        </form>
                    </c:if>
                    <c:if test = "${type == '2'}">
                        <div class="titleName"><c:out value="${item.name}"/></div>
                        <div><c:out value="${item.address}"/></div>
                        <form action="Server" method="post">
                            <input type="hidden" name="action" value="moreInfo"/>
                            <input type="hidden" name="type" value="company"/>
                            <input type="hidden" name="itemID" value="${item.id}"/>
                            <button onclick="sendOther('form${status.index}');">${content[4]}</button>
                        </form>
                    </c:if>
                    </div>
               	</c:forEach>
            </div>
           <c:if test="${allData.size() == 0}">
                <div style="width:900px; margin-left:100px; overflow:hidden;">
                    <p style="padding-top:.33em">
                    <c:out value="${content[5]}"/><b><c:out value="${searchData}"/></b>
                    <br><c:out value="${content[6]}"/></p>
                    <p style="margin-top:1em"><c:out value="${content[7]}"/></p>
                    <ul style="margin-left:1.3em;margin-bottom:2em">
                        <li><c:out value="${content[8]}"/></li>
                        <li><c:out value="${content[9]}"/></li>
                        <li><c:out value="${content[10]}"/></li>
                    </ul>
                </div>
           </c:if>
        </div>
        <div class="menuLineBrick">
            <c:if test="${empty role}">
                <form action="Server" method="get" id="registration">
                    <input type="hidden" name="page" value="registration"/>
                    <input type="button" value='${headers[9]}' class="button" id="but1" onclick="sendOther('registration');">
                </form>
                <form action="Server" method="get" id="authorization">
                    <input type="hidden" name="page" value="authorization"/>
                    <input type="button" value='${headers[10]}' class="button" id="but2" onclick="sendOther('authorization');">
                </form>
            </c:if>
            <c:if test="${role == 'user'}">
                <form action="Server" method="post" id="actionForm1">
                    <input value="${headers[18]}" type="submit">
                    <input type="hidden" name="action" value="act"/>
                    <input type="hidden" name="type" value="1"/>
                </form>
                <form action="Server" method="post" id="actionForm2">
                    <input value="${headers[19]}" type="submit">
                    <input type="hidden" name="action" value="act"/>
                    <input type="hidden" name="type" value="2"/>
                </form>
            </c:if>
            <form action="Server" method="post" id="languageForm">
                <input type="hidden" name="action" value="language"/>
                <span>${headers[1]}</span>
                <img src="${headers[2]}" height="25" width="25" alt="${headers[3]}">
                <select onChange="sendOther('languageForm');" name="type">
                    <option value="${headers[13]}">${headers[4]}</option>
                    <option value="${headers[14]}">${headers[5]}</option>
                </select>
            </form>
            <c:if test="${role == 'user'}">
                <form action="Server" method="post" id="endSessionForm">
                    <input type="hidden" name="action" value="endSession"/>
                    <input type="button" id="exit" value="${headers[17]}" onclick="sendOther('endSessionForm');"/>
                </form>
            </c:if>
        </div>
        <div class="searchMenu" id="searchMenuSkewBrick"></div>
        <div class="searchMenu" id="searchMenuBrick">
        <div id="pagename">
            <form action="Server" method="post" id="searchForm">
                <select size="1" name="type">
                    <option value="1">${headers[6]}</option>
                    <option value="2">${headers[7]}</option>
                </select>
                <input type="hidden" name="action" value="searchItems"/>
                <input type="text" name="searchData" size="20" class="in" id="searchID" value="${headers[8]}" onclick="backspaceAll('searchID');" onfocusout="reWrite('searchID');">
                <input type="button" value="${headers[15]}" id="sendButton" class="button" onclick="send('searchForm');">
            </form>
        </div>
    </div>
    </div>
    <div style="position:absolute; margin-top: 0;" id="start"></div>
    <div class="whiteShadow"></div>
    <div class="header">
        <form action="Server" method="get" id="startPage">
            <img src="../IMG/home.png" width="40" height="40" onclick="sendOther('startPage');">
        </form>
        ${headers[11]}
    </div>
    <div class="footer">${headers[12]}</div>
    <a href="#start"><div class="up">${headers[16]}</div></a>
</body>
    <script src="../js/search.js"></script>
    <script src="../js/items.js"></script>
</html>