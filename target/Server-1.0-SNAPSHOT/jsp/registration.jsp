﻿<%@ page language="java" contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>${content[0]}</title>
    <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style1.css" rel="stylesheet">
</head>
<body>
    <div class="tabloid"></div>
    <div class="under"></div>
    <div class="content">
        <div id="mainLayer">
            <div id="mess">
                <span id="inner_text_message"></span>
            </div>
            <c:if test="${not empty failed}">
                <c:if test="${failed == 'false'}">
                    <div class="text" style="width: 100%; height:auto; overflow:hidden;">${content[13]}</div>
                </c:if>
                <c:if test="${failed == 'true'}">
                    <div class="text" style="width: 100%; height:auto; overflow:hidden;">${content[12]}</div>
                </c:if>
            </c:if>
            <form class="table" action="Server" method="post" id="reg">
                <div>${content[1]}</div>
                <div>${content[2]}</div>
                <div class="tr">
                    <label>
                        <span>${content[3]}</span>
                        <input type="text" name="login" requaried size="25" class="in" id="Login" oninput="checkValidLogin('Login')" onfocusout="resultCheck('Login')">
                    </label>
                </div>
                <div class="tr">
                    <label>
                        <span>${content[4]}</span>
                        <input type="password" name="password" requaried size="25" class="in" id="Password" onfocusout="checkValidPassword('Password')">
                    </label>
                </div>
                <div class="tr">
                    <label>
                        <span>${content[5]}</span>
                        <input type="text" name="name" requaried size="25" class="in" id="Name" onfocusout="resultCheckName('Name')">
                    </label>
                </div>
                <div class="tr">
                    <label>
                        <span>${content[6]}</span>
                        <input type="text" name="surname" requaried size="25" class="in" id="Surname" onfocusout="resultCheckName('Surame')">
                    </label>
                </div>
                <div class="tr" id="EmailForms">
                    <label>
                        <span>${content[7]}</span>
                        <input type="text" name="email" size="25" class="in" id="Email" onchange="checkValidEmail('Email')">
                    </label>
                    <input type="button" value="${content[8]}" class="buttonMenu" id="add" onclick="addEmail('${content[11]}')">
                    <br>
                </div>
                <div class="tr">
                    <input type="hidden" name="action" value="registration"/>
                    <input type="button" name="registerUser" value="${content[9]}" class="button" onclick="checkAllForRegistration()">
                </div>
            </form>
        </div>
    </div>
    <div class="header">
        <form action="Server" method="get" id="startPage">
            <img src="../IMG/home.png" width="40" height="40" onclick="sendOther('startPage');">
        </form>
        ${content[10]}
    </div>
    <div class="footer">${headers[12]}</div>
</body>
    <script src="../js/validity.js"></script>
    <script src="../js/search.js"></script>
</html>