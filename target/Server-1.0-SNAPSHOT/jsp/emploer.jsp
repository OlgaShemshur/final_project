﻿<!DOCTYPE html>
<html>
<head lang="ru">
    <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style2.css" rel="stylesheet">
    <meta charset="UTF-8">
    <title>Работа, вакансии, резюме</title>
</head>
<body>
    <div class="menuLine"></div>
    <div class="searchMenu" id="menuBrick"></div>
    <div class="content">
        <div class="menuLineBrick">
            <select style = "background-color: #fcfcfc; color:#000000;">
                <option></option>
                <option>Вакансии</option>
                <option>Интервью</option>
            </select>
            <span>Язык/Language</span><img src="IMG/rus.png" height="25" width="25" alt="rus">
            <select>
                <option>Русский</option>
                <option>Английский</option>
            </select>
            <button id="exit">Выход</button>
        </div>
        <div class="searchMenu" id="searchMenuSkewBrick"></div>
        <div class="searchMenu" id="searchMenuBrick">
        <div id="pagename">
            <form action="Server" method="get" id="searchForm">
                <select size="1" name="type">
                    <option>Вакансии</option>
                    <option>Компании</option>
                    <option>Резюме</option>
                </select>
                <input type="hidden" name="action" value="search"/>
                <input type="text" name="search" size="20" class="in" id="search" value="Поиск..." onclick="backspaceAll('search');" onfocusout="reWrite('search');">
                <input type="button" value="Найти" id="♦" class="button" onclick="send('searchForm');">
            </form>
        </div>
        </div>
    </div>
    <div class="header">Найдите работу мечты</div>
    <div class="footer">2017 год</div>
</body>
    <script src="../js/search.js"></script>
</html>