﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="java.util.ArrayList"%>
<html>
<head lang="ru">
    <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style2.css" rel="stylesheet">
    <meta charset="UTF-8">
    <title>Работа, вакансии, резюме</title>
</head>
<body>
   <div class="menuLine"></div>
    <div class="searchMenu" id="menuBrick"></div>
    <div class="content">
        <div class="advertisement">
            <div>
                <div><a href='<c:out value="${advertisement[0]}"/>'><c:out value="${advertisement[1]}"/></a></div>
                <div><a href='<c:out value="${advertisement[2]}"/>'><c:out value="${advertisement[3]}"/></a></div>
                <div><a href='<c:out value="${advertisement[3]}"/>'><c:out value="${advertisement[5]}"/></a></div>
            </div>
        </div>
        <div id="mainLayer">
            <div class="result" id="result">
                <jsp:useBean id="ob" scope="page" class="servlet.presentationLayer.View" />
                <c:set var="item" value="${ob.getVacancyByID(id)}"/>
                <div>
                    <div class="titleName">${content[0]}<c:out value="${item.position}"/></div>
                    <div><b>${content[1]}
                    <c:choose>
                        <c:when test="${item.salary != 0.}">
                            <c:out value="${item.salary}"/> <c:out value="${item.salaryCurrency}"/>
                        </c:when>
                        <c:otherwise>${content[2]}</c:otherwise>
                    </c:choose>
                    </b></div>
                    <div><c:out value="${item.requirements}"/></div>
                    <c:set var="employer" value="${ob.getEmployerByID(item)}"/>
                    <c:set var="user" value="${ob.getUserByID(employer.idemployee)}"/>
                    <div>${content[3]}<c:out value="${employer.email}"/> <c:out value="${employer.telephoneNumber}"/></div>
                    <div><c:out value="${user.surname}"/> <c:out value="${user.name}"/></div>
                    <div><c:out value="${employer.other}"/></div>
                    <div class="link">
                    <c:set var="company" value="${ob.getCompanyByID(employer)}"/>
                        <form action="Server" method="post" id="companyForm">
                            <div onclick="sendOther('companyForm');">${content[4]}<c:out value="${company.name}"/></div>
                            <input type="hidden" name="action" value="moreInfo"/>
                            <input type="hidden" name="type" value="company"/>
                            <input type="hidden" name="itemID" value="${company.id}"/>
                        </form>
                    </div>
                    <div style="color:#730f35; background-color: #d6d6d6;">${content[5]}</div>
                    <div>${content[6]}<c:out value="${item.timeAdded}"/> id:<c:out value="${id}"/></div>
                </div>
            </div>
        </div>
        <div class="menuLineBrick">
            <a href="registration.jsp"><input type="button" value="${headers[9]}" class="button" style="margin-left: -300px; margin-top: -15px;"></a>
            <a href="authorization.jsp"><input type="button" value="${headers[10]}" class="button" style="margin-left: -125px; margin-top: -15px;"></a>
            <form action="Server" method="post" id="languageForm">
                <input type="hidden" name="action" value="language"/>
                <span>${headers[1]}</span>
                <img src="${headers[2]}" height="25" width="25" alt="${headers[3]}">
                <select onChange="sendOther('languageForm');" name="type">
                    <option value="${headers[13]}">${headers[4]}</option>
                    <option value="${headers[14]}">${headers[5]}</option>
                </select>
            </form>
        </div>
        <div class="searchMenu" id="searchMenuSkewBrick"></div>
        <div class="searchMenu" id="searchMenuBrick">
        <div id="pagename">
            <form action="Server" method="post" id="searchForm">
                <select size="1" name="type">
                    <option value="1">${headers[6]}</option>
                    <option value="2">${headers[7]}</option>
                </select>
                <input type="hidden" name="action" value="searchItems"/>
                <input type="text" name="searchData" size="20" class="in" id="searchID" value="${headers[8]}" onclick="backspaceAll('searchID');" onfocusout="reWrite('searchID');">
                <input type="button" value="${headers[15]}" id="sendButton" class="button" onclick="send('searchForm');">
            </form>
        </div>
        </div>
    </div>
    <div style="position:absolute; margin-top: 0;" id="start"></div>
    <div class="whiteShadow"></div>
    <div class="header">
        <form action="Server" method="get" id="startPage">
            <img src="../IMG/home.png" width="40" height="40" onclick="sendOther('startPage');">
        </form>
        ${headers[11]}
    </div>
    <div class="footer">${headers[12]}</div>
    <a href="#start"><div class="up">${headers[16]}</div></a>
</body>
    <script src="../js/search.js"></script>
</html>