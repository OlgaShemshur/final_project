﻿<%@ page language="java" contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.ArrayList"%>
<html>
<head>
    <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style2.css" rel="stylesheet">
    <link href="../css/style3.css" rel="stylesheet">
    <title><c:out value="${headers[0]}"/></title>
</head>
<body>
    <div class="menuLine"></div>
    <div class="searchMenu" id="menuBrick"></div>
    <div class="content">
        <div id="mainLayer">
    <c:if test="${role=='user'}">
           <div class="result" id="result">
    <jsp:useBean id="ob" scope="page" class="servlet.presentationLayer.View" />
    <c:set var="companies" value="${ob.getCompanyByCreatorID(user.id)}"/>
    <c:forEach var="item" items="${companies}" >
        <div>
        <div class="titleName"><c:out value="${item.position}"/></div>
        <div><b>${content[0]}
        <c:choose>
            <c:when test="${item.salary != 0.}">
                <c:out value="${item.salary}"/> <c:out value="${item.salaryCurrency}"/>
            </c:when>
            <c:otherwise>
                ${content[1]}
            </c:otherwise>
        </c:choose>
        </b></div>
        <div><c:out value="${item.requirements}"/></div>
        <c:set var="employer" value="${ob.getEmployerByID(item)}"/>
        <c:set var="company" value="${ob.getCompanyByID(employer)}"/>
        <div>${content[2]}<c:out value="${company.name}"/></div>
        <div>${content[3]}<c:out value="${item.timeAdded}"/></div>
        <form action="Server" method="post">
        <input type="hidden" name="action" value="moreInfo"/>
        <input type="hidden" name="itemID" value="${item.id}"/>
        <input type="hidden" name="type" value="vacancy"/>
        <button onclick="sendOther('form${status.index}');">${content[4]}</button>
        </form>
        </div>
    </c:forEach>
    <c:if test="${companies == null}">
        <div>${content[5]}</div>
    </c:if>
        </div>
        </c:if>
        <c:if test="${empty role}">
            <div class="text" style="width: 100%; height:auto; overflow:hidden; color: black;">${headers[20]}</div>
        </c:if>
        <c:if test="${role=='user'}">
        <form class="table" action="Server" method="post" id="reg">
            <img src="../IMG/cross.png" width="28" height="28" style="margin-left:415px; position: absolute;" onclick="hide('reg');"/>
            <div>${content[7]}</div>
            <div>${content[8]}</div>
            <div class="tr">
                <label>
                <span>${content[9]}</span>
                    <input type="text" name="login" requaried size="25" class="in" id="Login" oninput="checkValidLogin('Login')" onfocusout="resultCheck('Login')">
                </label>
            </div>
            <div class="tr">
                <label>
                    <span>${content[10]}</span>
                    <input type="password" name="password" requaried size="25" class="in" id="Password" onfocusout="checkValidPassword('Password')">
                </label>
            </div>
            <div class="tr">
                <input type="hidden" name="action" value="registration"/>
                <input type="button" name="registerCompany" value="${content[11]}" class="button" onclick="checkAllForRegistrationCompany()">
            </div>
        </form>
        </c:if>
        </div>
        <div class="menuLineBrick">
            <c:if test="${empty role}">
                <form action="Server" method="get" id="registration">
                    <input type="hidden" name="page" value="registration"/>
                    <input type="button" value='${headers[9]}' class="button" id="but1" onclick="sendOther('registration');">
                </form>
                <form action="Server" method="get" id="authorization">
                    <input type="hidden" name="page" value="authorization"/>
                    <input type="button" value='${headers[10]}' class="button" id="but2" onclick="sendOther('authorization');">
                </form>
            </c:if>
            <c:if test="${role == 'user'}">
                <button type="submit" style="color:white; background-color:darkred" onclick="show('reg');">${content[6]}</button>
                <form action="Server" method="post" id="actionForm2">
                    <input value="${headers[19]}" type="submit">
                    <input type="hidden" name="action" value="act"/>
                    <input type="hidden" name="type" value="2"/>
                </form>
            </c:if>
            <form action="Server" method="post" id="languageForm">
                <input type="hidden" name="action" value="language"/>
                <span>${headers[1]}</span>
                <img src="${headers[2]}" height="25" width="25" alt="${headers[3]}">
                <select onChange="sendOther('languageForm');" name="type">
                    <option value="${headers[13]}">${headers[4]}</option>
                    <option value="${headers[14]}">${headers[5]}</option>
                </select>
            </form>
            <c:if test="${role == 'user'}">
                <form action="Server" method="post" id="endSessionForm">
                    <input type="hidden" name="action" value="endSession"/>
                    <input type="button" id="exit" value="${headers[17]}" onclick="sendOther('endSessionForm');"/>
                </form>
            </c:if>
        </div>
        <div class="searchMenu" id="searchMenuSkewBrick"></div>
        <div class="searchMenu" id="searchMenuBrick">
        <div id="pagename">
            <form action="Server" method="post" id="searchForm">
                <select size="1" name="type">
                    <option value="1">${headers[6]}</option>
                    <option value="2">${headers[7]}</option>
                </select>
                <input type="hidden" name="action" value="searchItems"/>
                <input type="text" name="searchData" size="20" class="in" id="searchID" value="${headers[8]}" onclick="backspaceAll('searchID');" onfocusout="reWrite('searchID');">
                <input type="button" value="${headers[15]}" id="sendButton" class="button" onclick="send('searchForm');">
            </form>
        </div>
    </div>
    </div>
    <div style="position:absolute; margin-top: 0;" id="start"></div>
    <div class="whiteShadow"></div>
    <div class="header">
        <form action="Server" method="get" id="startPage">
            <img src="../IMG/home.png" width="40" height="40" onclick="sendOther('startPage');">
        </form>
        ${headers[11]}
    </div>
    <div class="footer">${headers[12]}</div>
    <a href="#start"><div class="up">${headers[16]}</div></a>
</body>
    <script src="../js/search.js"></script>
    <script src="../js/items.js"></script>
    <script src="../js/validity.js"></script>
</html>