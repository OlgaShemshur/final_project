package servlet.dao;

import servlet.businessLogic.entities.Employee;
import servlet.businessLogic.entities.Entity;
import java.sql.*;
import java.util.ArrayList;

public class DAOEmploee implements DAOInterface {
    @Override
    public boolean isExist() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        try  {
            DatabaseMetaData meta = connection.getMetaData();
            ResultSet res = meta.getTables(null, null, "emploee",
                    new String[] {"TABLE"});
            while (res.next()) {
                return true;
            }
            return false;
        }
        catch (SQLException e) {
            return false;
        }
        finally {
            ConnectionPool.closeConnection(connection);
        }
    }

    @Override
    public ArrayList<Entity> getEntityByField(String[] field, Object[] value) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM applicant_data where ";
        for(int i=0; i<field.length; i++){
            if(value[i] instanceof Integer || value[i] instanceof Float) sql += field[i] + " = " + value[i];
            else sql += field[i] + " = '" + value[i] + "'";
            if(i+1 != field.length) sql = " AND ";
        }
        sql += ";";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Employee employee = new Employee();
            employee.setIdapplicant(rs.getInt("idapplicant"));
            employee.setdateOfBrithday(rs.getDate("date_of_brithday"));
            employee.setSex(rs.getString("sex"));
            employee.setEmail(rs.getString("email"));
            employee.setTelephoneNumber(rs.getString("telephone_number"));
            employee.setEducation(rs.getString("education"));
            employee.setOther(rs.getString("other"));
            employee.setDefault();
            arrayList.add(employee);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }

    @Override
    public boolean deleteEntityByField(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        Employee employee = (Employee)entity;
        String sql = "DELETE FROM applicant_data where idapplicant = " + employee.getIdapplicant() + ";";
        statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }
    
    @Override
    public boolean createEntity(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
            Employee employee = (Employee)entity;
            String sql = "INSERT INTO applicant_data(idapplicant, date_of_brithday, sex, education, other, telephone_number, email) VALUES("
                    + employee.getIdapplicant() + ", '" + employee.getDateOfBirthday() + "', '" + employee.getSex()+ "', '"
                    + employee.getEducation() + "', '" + employee.getOther()+ "', '" + employee.getTelephoneNumber()+
                    "', '" + employee.getEmail() + "');";
            statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public ArrayList<Entity> getAll() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM applicant_data;";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Employee employee = new Employee();
            employee.setIdapplicant(rs.getInt("idapplicant"));
            employee.setdateOfBrithday(rs.getDate("date_of_brithday"));
            employee.setSex(rs.getString("sex"));
            employee.setEmail(rs.getString("email"));
            employee.setTelephoneNumber(rs.getString("telephone_number"));
            employee.setEducation(rs.getString("education"));
            employee.setOther(rs.getString("other"));
            employee.setDefault();
            arrayList.add(employee);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }
    
    @Override
    public boolean updateEntity(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
            Employee employee = (Employee)entity;
            String sql;
            if(employee.getDateOfBirthday()!=null)
            sql = "UPDATE applicant_data set date_of_brithday = '" + employee.getDateOfBirthday() + "', sex = '" + employee.getSex()+ "', email= '" + employee.getEmail()+
                    "', telephone_number = '" + employee.getTelephoneNumber()+ "', education = '" +
                    employee.getEducation().toString()+ "', other = '" + employee.getOther()+
                    "' where idapplicant = " + employee.getIdapplicant()+ "; ";
            else sql = "UPDATE applicant_data set sex = '" + employee.getSex()+ "', email= '" + employee.getEmail()+
                    "', telephone_number = '" + employee.getTelephoneNumber()+ "', education = '" +
                    employee.getEducation()+ "', other = '" + employee.getOther()+
                    "', where idapplicant = " + employee.getIdapplicant()+ "; ";
            statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public int getCount() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT COUNT(\"id\") AS 'COUNT' FROM applicant_data;";
        ResultSet rs = statement.executeQuery(sql);
        int count = 0;
        while(rs.next()){
            count = rs.getInt("COUNT");
        }
        ConnectionPool.closeConnection(connection);
        return count;
    }

    @Override
    public ArrayList<Entity> searchEntities(String field, String value) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM applicant_data where UPPER('"+ field +"') LIKE UPPER('%\" + value +\"%');";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Employee employee = new Employee();
            employee.setIdapplicant(rs.getInt("idapplicant"));
            employee.setdateOfBrithday(rs.getDate("date_of_brithday"));
            employee.setSex(rs.getString("sex"));
            employee.setEmail(rs.getString("email"));
            employee.setTelephoneNumber(rs.getString("telephone_number"));
            employee.setEducation(rs.getString("education"));
            employee.setOther(rs.getString("other"));
            employee.setDefault();
            arrayList.add(employee);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }
}
