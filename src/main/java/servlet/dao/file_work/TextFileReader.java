package servlet.dao.file_work;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TextFileReader implements Runnable {
    private Lock lock;
    private final String fileName;
    private String data;
    private ArrayList<String> allData = new ArrayList<>();
    private  ArrayList<String> escape = new ArrayList<>();

    public void setData(String data) {
        this.data = data;
    }

    public ArrayList<String> getAllData() {
        return allData;
    }

    public void setAllData(ArrayList<String> allData) {
        this.allData = allData;
    }

    public TextFileReader(String fileName){
        this.lock = new ReentrantLock();
        this.fileName = fileName;
        escape.add("\\;");
        escape.add("");
    }

    public String getFileName(){return fileName;}
    public String getData(){return data;}

    @Override
    public void run() {
        try {
            data = read();
            allData.addAll(Arrays.asList(data.split("\\;")));
            allData.removeAll(escape);
            for(int i=0; i < allData.size(); i++) {
                allData.set(i,allData.get(i)+"\\;");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String read() throws InterruptedException {
        lock.lock();
        String s = "";
        try(FileReader reader = new FileReader(fileName))
        {
            int c;
            int i = 0;
            while((c=reader.read())!=-1){
                char k = (char)c;
                s += k;
            }
            reader.close();
        }
        catch(IOException ex){
            //console.error(ex);
        }
        finally {
            lock.unlock();
        }
        return s;
    }
}
