package servlet.dao;

import servlet.businessLogic.entities.Entity;
import servlet.businessLogic.entities.Interview;
import servlet.businessLogic.entities.InterviewResult;

import java.sql.*;
import java.util.ArrayList;

public class DAOInterview implements DAOInterface {
    @Override
    public boolean isExist() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        try  {
            DatabaseMetaData meta = connection.getMetaData();
            ResultSet res = meta.getTables(null, null, "interview",
                    new String[] {"TABLE"});
            while (res.next()) {
                return true;
            }
            return false;
        }
        catch (SQLException e) {
            return false;
        }
        finally {
            ConnectionPool.closeConnection(connection);
        }
    }

    @Override
    public ArrayList<Entity> getEntityByField(String[] field, Object[] value) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM interview where ";
        for(int i=0; i<field.length; i++){
            if(value[i] instanceof Integer || value[i] instanceof Float) sql += field[i] + " = " + value[i];
            else sql += field[i] + " = '" + value[i] + "'";
            if(i+1 != field.length) sql = " AND ";
        }
        sql += ";";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Interview interview = new Interview();
            interview.setID(rs.getInt("id"));
            interview.setIdapplicant(rs.getInt("idapplicant"));
            interview.setIdvacancy(rs.getInt("idvacancy"));
            interview.setTimeOfInterview(rs.getTimestamp("time_of_interview"));
            interview.setTimeAdded(rs.getTimestamp("time_added"));
            interview.setIsActive(rs.getBoolean("isactive"));
            interview.setDefault();
            arrayList.add(interview);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }
    @Override
    public boolean deleteEntityByField(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "DELETE FROM interview where id = " + entity.getID() + ";";
        DAOInterviewResult daoInterviewResult = new DAOInterviewResult();
        InterviewResult ir = new InterviewResult();
        ir.setInterviewid(entity.getID());
        daoInterviewResult.deleteEntityByField(ir);
        statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }
    
    public boolean deleteEntityByVacancy(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        Interview interview = new Interview();
        String sql = "DELETE FROM interview where idvacancy = " + interview.getIdvacancy() + ";";
        DAOInterviewResult daoInterviewResult = new DAOInterviewResult();
        InterviewResult ir = new InterviewResult();
        ir.setInterviewid(entity.getID());
        daoInterviewResult.deleteEntityByField(ir);
        statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public boolean createEntity(Entity entitiy) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
            Interview interview = (Interview)entitiy;
            String sql;
            if(interview.getTimeOfInterview()!=null)
            sql = "INSERT INTO interview (idapplicant , idvacancy, time_of_interview) VALUES(" + 
                    interview.getIdapplicant() + ", " +interview.getIdvacancy() +", '" +  interview.getTimeOfInterview() + "');";
            else sql = "INSERT INTO interview (idapplicant , idvacancy) VALUES(" + 
                    interview.getIdapplicant() + ", " +interview.getIdvacancy() +");";
            statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public ArrayList<Entity> getAll() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM interview where;";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Interview interview = new Interview();
            interview.setIdapplicant(rs.getInt("idapplicant"));
            interview.setIdvacancy(rs.getInt("idvacancy"));
            interview.setTimeOfInterview(rs.getTimestamp("time_of_interview"));
            interview.setTimeAdded(rs.getTimestamp("time_added"));
            interview.setIsActive(rs.getBoolean("isactive"));
            interview.setDefault();
            arrayList.add(interview);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }

    @Override
    public boolean updateEntity(Entity entitiy) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
            Interview interview = (Interview)entitiy;
            String sql = "UPDATE interview set time_of_interview = '" + interview.getTimeOfInterview()+ "', isactive = '" + interview.getIsActive()+ "' where id = " + interview.getID() + "; ";
            statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public int getCount() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT COUNT(\"id\") AS 'COUNT' FROM interview;";
        ResultSet rs = statement.executeQuery(sql);
        int count = 0;
        while(rs.next()){
            count = rs.getInt("COUNT");
        }
        ConnectionPool.closeConnection(connection);
        return count;
    }

    @Override
    public ArrayList<Entity> searchEntities(String field, String value) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM interview where UPPER('"+ field +"') LIKE UPPER('%\" + value +\"%');";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Interview interview = new Interview();
            interview.setID(rs.getInt("id"));
            interview.setIdapplicant(rs.getInt("idapplicant"));
            interview.setIdvacancy(rs.getInt("idvacancy"));
            interview.setTimeOfInterview(rs.getTimestamp("time_of_interview"));
            interview.setTimeAdded(rs.getTimestamp("time_added"));
            interview.setIsActive(rs.getBoolean("isactive"));
            interview.setDefault();
            arrayList.add(interview);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }
}
