package servlet.dao;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import javax.sql.DataSource;
import javax.naming.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {

	private static BlockingQueue<Connection> connectionQueue;
	private static ConnectionPool pool;
	private static Lock lock;

	static {
		new DOMConfigurator().doConfigure("src\\main\\webapp\\WEB-INF\\log4j.xml", LogManager.getLoggerRepository());
		Logger file = Logger.getLogger("log");
		try {
			pool = new ConnectionPool(32);
		}catch (SQLException e) {
			file.error("Can't create connections");
		} catch (NamingException e) {
			file.error("No jdbs");
		}
	}

	private ConnectionPool(final int POOL_SIZE) throws SQLException, NamingException {
		lock = new ReentrantLock();
		connectionQueue = new ArrayBlockingQueue<Connection> (POOL_SIZE);
		Context envCtx = (Context) (new InitialContext().lookup("java:comp/env"));
		DataSource ds = (DataSource) envCtx.lookup("jdbc/HR_system");
		for (int i = 0; i < POOL_SIZE; i++) {
			Connection connection = ds.getConnection();
			connectionQueue.offer(connection);
		}
	}

	public static Connection getConnection() throws InterruptedException {
		lock.lock();
		Connection connection = null;
		try {
			connection = connectionQueue.take();
		} finally {
			lock.unlock();
		}
		return connection;
	}

	public static void closeConnection(Connection connection) {
		lock.lock();
		try {
			connectionQueue.offer(connection);
		} finally {
			lock.unlock();
		}
	}
}
