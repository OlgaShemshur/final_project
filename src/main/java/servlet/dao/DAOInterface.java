package servlet.dao;

import servlet.businessLogic.entities.Entity;

import java.sql.SQLException;
import java.util.ArrayList;

public interface DAOInterface {
    boolean isExist() throws SQLException, InterruptedException;
    ArrayList<Entity> getEntityByField(String[] field, Object[] value) throws SQLException, InterruptedException;
    ArrayList<Entity> getAll() throws SQLException, InterruptedException;
    boolean deleteEntityByField(Entity entity) throws SQLException, InterruptedException;
    boolean createEntity(Entity entitiy) throws SQLException, InterruptedException;
    boolean updateEntity(Entity entity) throws SQLException, InterruptedException;
    int getCount() throws SQLException, InterruptedException;
    ArrayList<Entity> searchEntities(String field, String value) throws SQLException, InterruptedException;
}
