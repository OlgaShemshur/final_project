package servlet.dao;

import servlet.businessLogic.entities.Company;
import servlet.businessLogic.entities.Employer;
import servlet.businessLogic.entities.Entity;

import java.sql.*;
import java.util.ArrayList;

public class DAOCompany implements DAOInterface {
    @Override
    public boolean isExist() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        try  {
            DatabaseMetaData meta = connection.getMetaData();
            ResultSet res = meta.getTables(null, null, "company",
                    new String[] {"TABLE"});
            while (res.next()) {
               return true;
            }
            return false;
        }
        catch (SQLException e) {
            return false;
        }
        finally {
            ConnectionPool.closeConnection(connection);
        }
    }

    @Override
    public ArrayList<Entity> getEntityByField(String[] field, Object[] value) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM company where ";
        for(int i=0; i<field.length; i++){
            if(value[i] instanceof Integer || value[i] instanceof Float) sql += field[i] + " = " + value[i];
            else sql += field[i] + " = '" + value[i] + "'";
            if(i+1 != field.length) sql = " AND ";
        }
        sql += ";";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Company company = new Company();
            company.setID(rs.getInt("id"));
            company.setName(rs.getString("name"));
            company.setAddress(rs.getString("adress"));
            company.setIduser(rs.getInt("id_user"));
            company.setDefault();
            arrayList.add(company);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }
    @Override
    public boolean deleteEntityByField(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        PreparedStatement pstmt = connection.prepareStatement("DELETE FROM company where id = ?");
        pstmt.setInt(1, entity.getID());
        DAOEmployer daoEmployer = new DAOEmployer();
        Employer employer = new Employer();
        employer.setIdcompany(entity.getID());
        daoEmployer.deleteEntityByCompany(employer);
        pstmt.execute();
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public boolean createEntity(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
            Company company = (Company)entity;
            String sql = "INSERT INTO company (name, adress, id_user) VALUES('" +
                    company.getName() + "', '" + company.getAddress()+ "', " + company.getIduser()+ ");";
            statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public ArrayList<Entity> getAll() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM company;";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Company company = new Company();
            company.setID(rs.getInt("id"));
            company.setName(rs.getString("name"));
            company.setAddress(rs.getString("adress"));
            company.setDefault();
            arrayList.add(company);
        }
        ConnectionPool.closeConnection(connection);
            return arrayList;
    }

    @Override
    public boolean updateEntity(Entity entity) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
            Company company = (Company)entity;
            String sql = "UPDATE company set name = '" + company.getName() + "', adress = '" + company.getAddress()+ "' where id = " + company.getID() + "; ";
            statement.execute(sql);
        ConnectionPool.closeConnection(connection);
        return true;
    }

    @Override
    public int getCount() throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT COUNT(\"id\") AS 'COUNT' FROM company;";
        ResultSet rs = statement.executeQuery(sql);
        int count = 0;
        while(rs.next()){
            count = rs.getInt("COUNT");
        }
        ConnectionPool.closeConnection(connection);
        return count;
    }

    @Override
    public ArrayList<Entity> searchEntities(String field, String value) throws SQLException, InterruptedException {
        Connection connection = ConnectionPool.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM company where "+ field +" LIKE '%" + value +"%';";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Entity> arrayList = new ArrayList<>();
        while(rs.next()){
            Company company = new Company();
            company.setID(rs.getInt("id"));
            company.setName(rs.getString("name"));
            company.setAddress(rs.getString("adress"));
            company.setDefault();
            arrayList.add(company);
        }
        ConnectionPool.closeConnection(connection);
        return arrayList;
    }
}
