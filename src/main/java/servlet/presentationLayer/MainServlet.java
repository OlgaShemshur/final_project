package servlet.presentationLayer;

import org.xml.sax.SAXException;
import servlet.businessLogic.command.*;
import servlet.businessLogic.command.comandEntities.ICommand;
import servlet.dao.file_work.DOMParser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import java.io.IOException;
import java.sql.SQLException;

public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        HttpSession session = req.getSession(true);
        increaseCounter(session);
        checkLanguageAttribute(session);
        Receiver receiver = new Receiver(req, resp);
        Client client = new Client(receiver);
        ICommand command = null;
        if (req.getParameter("page") == null) {
            command = client.initCommand(TypeCommand.START_PAGE);
        } else command = client.initCommand(TypeCommand.SEND_PAGE);
        Invoker invoker = new Invoker(command);
        try {
            invoker.invokeCommand();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession(true);
        increaseCounter(session);
        checkLanguageAttribute(session);
        Receiver receiver = new Receiver(req, resp);
        Client client = new Client(receiver);
        ICommand command = null;
        if (req.getParameter("action") != null) {
            if (req.getParameter("action").equals("authorization")) {
                command = client.initCommand(TypeCommand.AUTHORIZATION);
            } else if (req.getParameter("action").equals("registration")) {
                command = client.initCommand(TypeCommand.REGISTRATION);
            } else if (req.getParameter("action").toString().equals("searchItems")) {
                command = client.initCommand(TypeCommand.SEARCH);
            } else if (req.getParameter("action").toString().equals("endSession")) {
                command = client.initCommand(TypeCommand.CLOSE);
            }  else if (req.getParameter("action").toString().equals("moreInfo")) {
                command = client.initCommand(TypeCommand.MORE_DATA);
            } else if (req.getParameter("action").toString().equals("delete")) {
                command = client.initCommand(TypeCommand.DELETE);
            } else if (req.getParameter("action").toString().equals("add")) {
                command = client.initCommand(TypeCommand.ADD);
            }  else if (req.getParameter("action").toString().equals("update")) {
                command = client.initCommand(TypeCommand.UPDATE);
            } else if (req.getParameter("action").toString().equals("language")) {
                command = client.initCommand(TypeCommand.CHANGE_LANGUAGE);
            } else command = client.initCommand(TypeCommand.ERROR);
        } else {
            command = client.initCommand(TypeCommand.ERROR);
        }
        try {
            Invoker invoker = new Invoker(command);
            invoker.invokeCommand();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    private void increaseCounter(HttpSession session) {
        Integer counter = (Integer) session.getAttribute("counter");
        if (counter == null) {
            session.setAttribute("counter", 1);
        } else {
            counter++;
            session.setAttribute("counter", counter);
        }
    }

    private void checkLanguageAttribute(HttpSession session){
        if (session.getAttribute("language") == null) {
            session.setAttribute("language", "ru");
            DOMParser domParserForAdvertisement = null;
            try {
                domParserForAdvertisement = new DOMParser("src//main//resources//xml//" + session.getAttribute("language") + "//advertisement.xml");
                session.setAttribute("advertisement", domParserForAdvertisement.getValues());
                DOMParser domParserForHeaders = new DOMParser("src//main//resources//xml//" + session.getAttribute("language") + "//headers.xml");
                session.setAttribute("headers", domParserForHeaders.getValues());
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}