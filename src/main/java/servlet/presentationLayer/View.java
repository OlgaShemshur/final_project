package servlet.presentationLayer;

import servlet.businessLogic.entities.*;
import servlet.businessLogic.logic.DataGet;

import java.sql.SQLException;
import java.util.ArrayList;

public class View {
    public static Company getCompanyByID(Employer employer) throws SQLException, InterruptedException {
        try{
            String[] fields = {"id"};
            Object[] values = {employer.getIdcompany()};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new Company(), fields, values);
            if(arrayList!=null && arrayList.size()>0)
                return (Company) arrayList.get(0);
            else return null;
        }
            catch (Exception exeption) {
            return null;
        }
    }

    public static Company getCompanyById(int id) throws SQLException, InterruptedException {
        try{
            String[] fields = {"id"};
            Object[] values = {id};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new Company(), fields, values);
            if(arrayList!=null && arrayList.size()>0)
                return (Company) arrayList.get(0);
            else return null;
        }
        catch (Exception exeption) {
            return null;
        }
    }

    public static ArrayList<Company> getCompaniesByCreatorID(Integer id) throws SQLException, InterruptedException {
        try{
            String[] fields = {"id_user"};
            Object[] values = {id};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new Company(), fields, values);
            ArrayList<Company> arrayListCompany = new ArrayList<>();
            for (Entity item : arrayList) {
                arrayListCompany.add((Company)item);
            }
            return arrayListCompany;
        }
        catch (Exception exeption) {
            return null;
        }
    }

    public static ArrayList<Employer> getEmployersByCompanyID(Integer id) throws SQLException, InterruptedException {
        try{
            String[] fields = {"idcompany"};
            Object[] values = {id};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new Employer(), fields, values);
            ArrayList<Employer> arrayListEmployer = new ArrayList<>();
            for (Entity item : arrayList) {
                arrayListEmployer.add((Employer)item);
            }
            return arrayListEmployer;
        }
        catch (Exception exeption) {
            return null;
        }
    }

    public static Employer getEmployerByID(Vacancy vacancy) throws SQLException, InterruptedException {
        try{
            String[] fields = {"idemployee"};
            Object[] values = {vacancy.getIdEmploee()};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new Employer(), fields, values);
            if(arrayList!=null && arrayList.size()>0)
                return (Employer) arrayList.get(0);
            else return null;
        }
        catch (Exception exeption) {
            return null;
        }
    }

    public static Employer getEmployerByID(int id) throws SQLException, InterruptedException {
        try{
            String[] fields = {"idemployee"};
            Object[] values = {id};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new Employer(), fields, values);
            if(arrayList!=null && arrayList.size()>0)
                return (Employer) arrayList.get(0);
            else return null;
        }
        catch (Exception exeption) {
            return null;
        }
    }

    public static Vacancy getVacancyByID(int id) throws SQLException, InterruptedException {
        try{
            String[] fields = {"id"};
            Object[] values = {id};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new Vacancy(), fields, values);
            if(arrayList!=null && arrayList.size()>0)
                return (Vacancy) arrayList.get(0);
            else return null;
        }
        catch (Exception exeption) {
            return null;
        }
    }

    public static Employee getEmployeeById(int id) throws SQLException, InterruptedException {
        try{
            String[] fields = {"idapplicant"};
            Object[] values = {id};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new Employee(), fields, values);
            if(arrayList!=null && arrayList.size()>0)
                return (Employee) arrayList.get(0);
            else return null;
        }
        catch (Exception exeption) {
            return null;
        }
    }

    public static User getUserByID(int id) throws SQLException, InterruptedException {
        try{
            String[] fields = {"id"};
            Object[] values = {id};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new User(), fields, values);
            if(arrayList!=null && arrayList.size()>0)
                return (User) arrayList.get(0);
            else return null;
        }
        catch (Exception exeption) {
            return null;
        }
    }

    public static User getUserByLogin(String login) throws SQLException, InterruptedException {
        try{
            String[] fields = {"login"};
            Object[] values = {login};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new User(), fields, values);
            if(arrayList!=null && arrayList.size()>0)
                return (User) arrayList.get(0);
            else return null;
        }
        catch (Exception exeption) {
            return null;
        }
    }

    public static ArrayList<Interview> getInterviewsByIDofEmployee(int id) throws SQLException, InterruptedException {
        try{
            String[] fields = {"idapplicant"};
            Object[] values = {id};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new Interview(), fields, values);
            ArrayList<Interview> arrayListInerview = new ArrayList<>();
            for (Entity item : arrayList) {
                arrayListInerview.add((Interview)item);
            }
            return arrayListInerview;
        }
        catch (Exception exeption) {
            return null;
        }
    }

    public static Interview getInterviewByID(int id) throws SQLException, InterruptedException {
        try{
            String[] fields = {"id"};
            Object[] values = {id};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new Interview(), fields, values);
            ArrayList<Interview> arrayListInerview = new ArrayList<>();
            for (Entity item : arrayList) {
                arrayListInerview.add((Interview)item);
            }
            return arrayListInerview.get(0);
        }
        catch (Exception exeption) {
            return null;
        }
    }

    public static ArrayList<InterviewResult> getInterviewResultsByIDofEmployee(int id) throws SQLException, InterruptedException {
        try{
            String[] fields = {"idapplicant"};
            Object[] values = {id};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new Interview(), fields, values);
            ArrayList<InterviewResult> arrayListResult = new ArrayList<>();
            for (Entity item : arrayList) {
                String[] fieldsTmp = {"idinterview"};
                Object[] valuesTmp = {item.getID()};
                ArrayList<Entity> tmp = DataGet.getALLByField(new InterviewResult(), fieldsTmp, valuesTmp);
                for (Entity itemTmp : tmp) {
                    arrayListResult.add((InterviewResult) itemTmp);
                }
            }
            return arrayListResult;
        }
        catch (Exception exeption) {
            return null;
        }
    }

    public static InterviewResult getInterviewResultByID(int id) throws SQLException, InterruptedException {
        try{
            String[] fields = {"id"};
            Object[] values = {id};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new Interview(), fields, values);
            ArrayList<InterviewResult> arrayListResult = new ArrayList<>();
           for (Entity itemTmp : arrayList) {
                    arrayListResult.add((InterviewResult) itemTmp);
            }
            return arrayListResult.get(0);
        }
        catch (Exception exeption) {
            return null;
        }
    }

    public static Integer getCountOfInterviewsByIDofEmployee(int id) throws SQLException, InterruptedException {
        try{
            String[] fields = {"idapplicant"};
            Object[] values = {id};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new Interview(), fields, values);
            return arrayList.size();
        }
        catch (Exception exeption) {
            return 0;
        }
    }

    public static Integer getCountOfInterviewResultsByIDofEmployee(int id) throws SQLException, InterruptedException {
        try{
            String[] fields = {"idapplicant"};
            Object[] values = {id};
            ArrayList<Entity> arrayList = DataGet.getALLByField(new Interview(), fields, values);
            ArrayList<InterviewResult> arrayListResult = new ArrayList<>();
            for (Entity item : arrayList) {
                String[] fieldsTmp = {"idinterview"};
                Object[] valuesTmp = {item.getID()};
                ArrayList<Entity> tmp = DataGet.getALLByField(new InterviewResult(), fieldsTmp, valuesTmp);
                for (Entity itemTmp : tmp) {
                    arrayListResult.add((InterviewResult) itemTmp);
                }
            }
            return arrayListResult.size();
        }
        catch (Exception exeption) {
            return 0;
        }
    }
}