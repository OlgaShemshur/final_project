package servlet.businessLogic.entities;

import java.io.Serializable;
import java.sql.Date;

public class Employee extends User implements Serializable {
    private enum Sex {
        men("м"), women("ж"), unknown("н");
        private final String text;

        private Sex(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    };

    private enum Education {
        high("высшее"), middleSpecial("среднее специальное"), middleTechnical("среднее техническое"), middle("среднее"), unknown("не указано");
        private final String text;
        
        private Education(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    };

    private Integer idApplicant;
    private Date dateOfBirthday;
    private Sex sex;
    private String email;
    private String telephoneNumber;
    private Education education;
    private String other;

    public Employee() {
        super();
        this.idApplicant = 0;
        this.dateOfBirthday = new Date(0, 0, 0);
        this.sex = Sex.unknown;
        this.email = "";
        this.telephoneNumber = "";
        this.education = Education.unknown;
        this.other = "";
    }

    @Override
    public void setDefault() {
        super.setDefault();
        if (this.idApplicant == null) this.idApplicant = 0;
        if (this.dateOfBirthday == null) this.dateOfBirthday = new Date(0, 0, 0);
        if (this.sex == null) this.sex = Sex.unknown;
        if (this.email == null) this.email = "";
        if (this.telephoneNumber == null) this.telephoneNumber = "";
        if (this.education == null) this.education = Education.unknown;
        if (this.other == null) this.other = "";
    }

    public String getOther() {
        return other;
    }

    public String getEducation() {
        return education.toString();
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getSex() {
        return sex.toString();
    }

    public Date getdateOfBrithday() {
        return dateOfBirthday;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public void setEducation(String education) {
        if (education != null) {
            if (education.equals("высшее")) setEducation(Education.high);
            else if (education.equals("среднее специальное")) setEducation(Education.middleSpecial);
            else if (education.equals("среднее техническое")) setEducation(Education.middleTechnical);
            else if (education.equals("среднее")) setEducation(Education.middle);
            else if (education.equals("не указано")) setEducation(Education.unknown);
        }
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSex(String sex) {
        if (sex == null) return;
        if (sex.equals("м")) setSex(Sex.men);
        else if (sex.equals("ж")) setSex(Sex.women);
        else setSex(Sex.unknown);
    }

    public void setdateOfBrithday(Date dateOfBrithday) {
        this.dateOfBirthday = dateOfBrithday;
    }

    public Date getDateOfBirthday() {
        return dateOfBirthday;
    }

    public Integer getIdapplicant() {
        return idApplicant;
    }

    public void setIdapplicant(Integer idapplicant) {
        this.idApplicant = idapplicant;
    }
}