package servlet.businessLogic.entities;

import java.io.Serializable;
import java.sql.Timestamp;

public class InterviewResult extends Entity implements Serializable {
    private Integer interviewid;
    private String recall;
    private Timestamp timeAdded;
    private Double averageMark;

    public InterviewResult() {
        super();
        this.interviewid = 0;
        this.recall = "";
        this.timeAdded = new Timestamp(0, 0, 0, 0, 0, 0, 0);
        this.averageMark = 0.;
    }

    @Override
    public void setDefault() {
        super.setDefault();
        if (this.interviewid == null) this.interviewid = 0;
        if (this.recall == null) this.recall = "";
        if (this.timeAdded == null) this.timeAdded = new Timestamp(0, 0, 0, 0, 0, 0, 0);
        if (this.averageMark == null) this.averageMark = 0.;
    }

    public Integer getId() {
        return super.getID();
    }

    public String getRecall() {
        return recall;
    }

    public Timestamp getTimeAdded() {
        return timeAdded;
    }

    public Double getAverageMark() {
        return averageMark;
    }

    public Integer getInterviewid() {
        return interviewid;
    }

    public void setInterviewid(Integer interviewid) {
        this.interviewid = interviewid;
    }

    public void setRecall(String recall) {
        this.recall = recall;
    }

    public void setTimeAdded(Timestamp timeAdded) {
        this.timeAdded = timeAdded;
    }

    public void setAverageMark(Double averageMark) {
        this.averageMark = averageMark;
    }
}
