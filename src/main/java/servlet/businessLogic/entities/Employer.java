package servlet.businessLogic.entities;

import java.io.Serializable;

public class Employer extends Entity implements Serializable {

    private Integer idemployee;
    private Integer idcompany;
    private String email;
    private String telephoneNumber;
    private String other;

    public Employer() {
        super();
        this.idemployee = 0;
        this.idcompany = 0;
        this.email = "";
        this.telephoneNumber = "";
        this.other = "";
    }

    @Override
    public void setDefault() {
        super.setDefault();
        if (this.idemployee == null) this.idemployee = 0;
        if (this.idcompany == null) this.idcompany = 0;
        if (this.email == null) this.email = "";
        if (this.telephoneNumber == null) this.telephoneNumber = "";
        if (this.other == null) this.other = "";
    }

    public Integer getIdemployee() {
        return idemployee;
    }

    public void setIdemployee(Integer idemployee) {
        this.idemployee = idemployee;
    }

    public Integer getIdcompany() {
        return idcompany;
    }

    public void setIdcompany(Integer idcompany) {
        this.idcompany = idcompany;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }
}
