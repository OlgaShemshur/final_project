package servlet.businessLogic.entities;

import java.io.Serializable;
import java.sql.Timestamp;

public class Interview extends Entity implements Serializable {
    private Integer idapplicant;
    private Integer idvacancy;
    private Timestamp timeOfInterview;
    private Timestamp timeAdded;
    private boolean isActive;

    public Interview() {
        super();
        this.idapplicant = 0;
        this.idvacancy = 0;
        this.timeOfInterview = null;
        this.timeAdded = null;
        this.isActive = false;
    }

    @Override
    public void setDefault() {
        super.setDefault();
        if (this.idapplicant == null) this.idapplicant = 0;
        if (this.idvacancy == null) this.idvacancy = 0;
        if (this.timeOfInterview == null) this.timeOfInterview = null;
        if (this.timeAdded == null) this.timeAdded = null;
    }

    public Integer getId() {
        return super.getID();
    }

    public Integer getIdapplicant() {
        return idapplicant;
    }

    public Integer getIdvacancy() {
        return idvacancy;
    }

    public Timestamp getTimeOfInterview() {
        return timeOfInterview;
    }

    public Timestamp getTimeAdded() {
        return timeAdded;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIdapplicant(Integer idapplicant) {
        this.idapplicant = idapplicant;
    }

    public void setIdvacancy(Integer idvacancy) {
        this.idvacancy = idvacancy;
    }

    public void setSurname(Timestamp timeOfInterview) {
        this.timeOfInterview = timeOfInterview;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public void setTimeOfInterview(Timestamp timeOfInterview) {
        this.timeOfInterview = timeOfInterview;
    }

    public void setTimeAdded(Timestamp timeAdded) {
        this.timeAdded = timeAdded;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
}
