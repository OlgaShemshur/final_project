package servlet.businessLogic.logic.Registration;

import java.sql.SQLException;

public interface RegistrationInterface {
    public boolean checkMatchInDB() throws SQLException, InterruptedException;
    public boolean register() throws SQLException, InterruptedException;
}
