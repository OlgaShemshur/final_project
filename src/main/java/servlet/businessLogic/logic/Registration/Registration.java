package servlet.businessLogic.logic.Registration;

import servlet.businessLogic.entities.Employee;
import servlet.businessLogic.entities.Employer;
import servlet.businessLogic.entities.Entity;
import servlet.businessLogic.entities.User;
import servlet.businessLogic.logic.Authorization;
import servlet.businessLogic.logic.DataGet;
import servlet.dao.DAOUser;

import java.sql.SQLException;
import java.util.ArrayList;

import static servlet.presentationLayer.View.getUserByLogin;

public class Registration extends Authorization implements RegistrationInterface {

    private Employee employee;
    private Employer employer;

    public Registration(String password, String login, String email, String name, String surname, String role) {
        super(password, login);
        getUser().setName(name);
        getUser().setSurname(surname);
        getUser().setRole(role);
        employee = new Employee();
        employer = new Employer();
        if (role != null && role.equals("соискатель")) employee.setEmail(email);
        else if (role != null && role.equals("сотрудник")) employer.setEmail(email);
    }

    public Registration(String password, String login, String email, String name, String surname, String role, Integer idCompany) {
        super(password, login);
        getUser().setName(name);
        getUser().setSurname(surname);
        getUser().setRole(role);
        employee = new Employee();
        employer = new Employer();
        if (role != null && role.equals("соискатель")) employee.setEmail(email);
        else if (role != null && role.equals("сотрудник")) {
            employer.setEmail(email);
            employer.setIdcompany(idCompany);
        }
    }

    @Override
    public boolean checkMatchInDB() throws SQLException, InterruptedException {
        DAOUser data = new DAOUser();
        String[] field = {"login"};
        String[] value = {getUser().getLogin()};
        ArrayList<Entity> arrayList = data.getEntityByField(field, value);
        if (arrayList.size() == 1) return true;
        else return false;
    }

    @Override
    public boolean register() throws SQLException, InterruptedException {
        if (checkValidityLogin() && checkValidityPassword() && !checkMatchInDB()) {
            if (!getLogin().isEmpty() && (getUser().getRole().equals("соискатель") || getUser().getRole().equals("сотрудник"))) {
                DataGet.createEntity(getUser());
                User tmp = (User) getUserByLogin(getLogin());
                if (getUser().getRole().equals("соискатель")) {
                    employee.setIdapplicant(tmp.getID());
                    DataGet.createEntity(employee);
                } else if (getUser().getRole().equals("сотрудник")) {
                    employer.setIdemployee(tmp.getID());
                    DataGet.createEntity(employer);
                }
                return true;
            } else return false;
        } else return false;
    }

}
