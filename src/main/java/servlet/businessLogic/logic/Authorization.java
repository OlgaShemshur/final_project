package servlet.businessLogic.logic;

import servlet.businessLogic.entities.Entity;
import servlet.businessLogic.entities.User;
import servlet.dao.DAOUser;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Authorization {
    private User user;
    private String loginRegularExpression = "([_A-Za-z\\d]{4,})";
    private String passwordRegularExpression = "([A-Za-z\\d\\W]{6,})";

    public Authorization(String password, String login) {
        user = new User();
        user.setPassword(password);
        user.setLogin(login);
    }

    public boolean checkValidityLogin() {
        if (user.getLogin() == null) return false;
        Pattern p = Pattern.compile(loginRegularExpression);
        Matcher m = p.matcher(user.getLogin());
        return m.matches();
    }

    public boolean checkValidityPassword() {
        if (user.getPassword() == null) return false;
        Pattern p = Pattern.compile(passwordRegularExpression);
        Matcher m = p.matcher(user.getPassword());
        return m.matches();
    }

    public boolean checkMatchInDB() throws SQLException, InterruptedException {
        DAOUser data = new DAOUser();
        String[] field = {"login", "password"};
        String[] value = {user.getLogin(), user.getPassword()};
        ArrayList<Entity> arrayList = data.getEntityByField(field, value);
        if (arrayList.size() == 1) return true;
        else return false;
    }

    public Entity getUserInDB() throws SQLException, InterruptedException {
        DAOUser data = new DAOUser();
        String[] field = {"login", "password"};
        String[] value = {user.getLogin(), user.getPassword()};
        ArrayList<Entity> arrayList = data.getEntityByField(field, value);
        if (arrayList.size() == 1) return arrayList.get(0);
        else return null;
    }

    public User getUser() {
        return user;
    }

    public String getLogin() {
        return user.getLogin();
    }

    public void setLogin(String login) {
        user.setLogin(login);
    }

    public String getPassword() {
        return user.getPassword();
    }

    public void setPassword(String password) {
        user.setPassword(password);
    }
}
