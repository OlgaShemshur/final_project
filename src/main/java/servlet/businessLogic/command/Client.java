package servlet.businessLogic.command;

import servlet.businessLogic.command.comandEntities.*;
import servlet.businessLogic.command.comandEntities.Error;

public class Client {
    private Receiver mReceiver;
    public Client(Receiver receiver) {
        mReceiver = receiver;
    }
    public ICommand initCommand(TypeCommand cmd) {
        ICommand command = null;
        switch(cmd) {
            case AUTHORIZATION:
                command = new AuthorizationUser(mReceiver);
                break;
            case REGISTRATION:
                command = new RegistrationUser(mReceiver);
                break;
            case SEARCH:
                command = new Search(mReceiver);
                break;
            case CLOSE:
                command = new CloseSession(mReceiver);
                break;
            case START_PAGE:
                command = new StartPageSend(mReceiver);
                break;
            case ERROR:
                command = new Error(mReceiver);
                break;
            case MORE_DATA:
                command = new MoreData(mReceiver);
                break;
            case DELETE:
                command = new Delete(mReceiver);
                break;
            case ADD:
                command = new MoreData(mReceiver);
                break;
            case UPDATE:
                command = new Update(mReceiver);
                break;
            case CHANGE_LANGUAGE:
                command = new ChangeLanguage(mReceiver);
                break;
            case SEND_PAGE:
                command = new SendPage(mReceiver);
                break;
            default:
                command = new Error(mReceiver);
        }
        return command;
    }
}
