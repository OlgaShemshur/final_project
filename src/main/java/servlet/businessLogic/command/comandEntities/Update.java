package servlet.businessLogic.command.comandEntities;

import org.xml.sax.SAXException;
import servlet.businessLogic.command.Receiver;
import servlet.businessLogic.command.TypeCommand;

import javax.servlet.ServletException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.SQLException;

public class Update implements ICommand {
    private Receiver receiver;
    public Update(Receiver reciever) {
        this.receiver = reciever;
    }
    @Override
    public void execute() throws SQLException, InterruptedException, ServletException, IOException, ParserConfigurationException, SAXException {
        receiver.action(TypeCommand.UPDATE);
    }
}
