window.onload = function() {
    var item = document.getElementById('result');
    if(item==null) return;
    var formsCollection = item.getElementsByTagName("form");
    for(var i=0;i<formsCollection.length;i++)
    {
        formsCollection[i].id="form"+i;
        var buttonCollection = formsCollection[i].getElementsByTagName("button");
        if(buttonCollection[0]!=null) buttonCollection[0].id="moreInfoButton"+i;
    }
}

function showAll() {
    var item = document.getElementById('result').style.opacity = "1";
}

function change(text, text1) {
    var item = document.getElementById('more');
    if(item.innerHTML != text)item.innerHTML=text;
    else {
        item.innerHTML = text1;
        document.getElementById('result').style.opacity = "0";
    }
}
