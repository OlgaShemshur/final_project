﻿<%@ page language="java" contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.ArrayList"%>
<html>
<head>
    <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style2.css" rel="stylesheet">
    <title><c:out value="${headers[0]}"/></title>
</head>
<body>
    <div class="menuLine"></div>
    <div class="searchMenu" id="menuBrick"></div>
    <div class="content">
        <div id="mainLayer">
           <div class="result" id="result">
    <c:if test="${role=='user'}">
    <jsp:useBean id="ob" scope="page" class="servlet.presentationLayer.View" />
    <c:set var="interviewResults" value="${ob.getInterviewResultsByIDofEmployee(user.id)}"/>
    <c:forEach var="result" items="${interviewResults}" >
        <c:set var="item" value="${ob.getInterviewByID(result.interviewid)}"/>
        <div>
        <c:set var="vacancy" value="${ob.getVacancyByID(item.idvacancy)}"/>
        <div class="titleName"><c:out value="${vacancy.position}"/></div>
        <div><b>${content[0]}
        <c:choose>
            <c:when test="${vacancy.salary != 0.}">
                <c:out value="${vacancy.salary}"/> <c:out value="${vacancy.salaryCurrency}"/>
            </c:when>
            <c:otherwise>
                ${content[1]}
            </c:otherwise>
        </c:choose>
        </b></div>
        <c:set var="employer" value="${ob.getEmployerByID(vacancy)}"/>
        <c:set var="company" value="${ob.getCompanyByID(employer)}"/>
        <div>${content[2]}<c:out value="${company.name}"/></div>
        <div>${content[3]}<c:out value="${item.timeOfInterview}"/></div>
        <div style="background-color: #2e572d; color: white;">${content[4]}<c:out value="${result.recall}"/></div>
        <div style="background-color: #2e572d; color: white;">${content[5]}<c:out value="${result.averageMark}"/></div>
        <div>${content[6]}<c:out value="${result.timeAdded}"/></div>
        <div>
        <form action="Server" method="post" class="line">
        <input type="hidden" name="action" value="moreInfo"/>
        <input type="hidden" name="itemID" value="${result.interviewid}"/>
        <input type="hidden" name="type" value="interview"/>
        <button onclick="sendOther('form${status.index}');">${content[7]}</button>
        </form>
        </div>
        </div>
    </c:forEach>
    <c:if test="${interviewResults.size() == 0}">
        <div>${content[8]}</div>
    </c:if>
        </c:if>
    <c:if test="${empty role}">
        <div class="text" style="width: 100%; height:auto; overflow:hidden; color: black;">${headers[20]}</div>
    </c:if>
    </div>
        </div>
        <div class="menuLineBrick">
            <c:if test="${empty role}">
                <form action="Server" method="get" id="registration">
                    <input type="hidden" name="page" value="registration"/>
                    <input type="button" value='${headers[9]}' class="button" id="but1" onclick="sendOther('registration');">
                </form>
                <form action="Server" method="get" id="authorization">
                    <input type="hidden" name="page" value="authorization"/>
                    <input type="button" value='${headers[10]}' class="button" id="but2" onclick="sendOther('authorization');">
                </form>
            </c:if>
            <c:if test="${role == 'user'}">
                <form action="Server" method="get" id="companies">
                    <input value="${headers[18]}" type="submit">
                    <input type="hidden" name="page" value="allCompanies"/>
                </form>
                <form action="Server" method="get" id="resume">
                    <input value="${headers[19]}" type="submit">
                    <input type="hidden" name="page" value="resume"/>
                </form>
            </c:if>
            <form action="Server" method="post" id="languageForm">
                <input type="hidden" name="action" value="language"/>
                <span>${headers[1]}</span>
                <img src="${headers[2]}" height="25" width="25" alt="${headers[3]}">
                <select onChange="sendOther('languageForm');" name="type">
                    <option value="${headers[13]}">${headers[4]}</option>
                    <option value="${headers[14]}">${headers[5]}</option>
                </select>
            </form>
            <c:if test="${role == 'user'}">
                <form action="Server" method="post" id="endSessionForm">
                    <input type="hidden" name="action" value="endSession"/>
                    <input type="button" id="exit" value="${headers[17]}" onclick="sendOther('endSessionForm');"/>
                </form>
            </c:if>
        </div>
        <div class="searchMenu" id="searchMenuSkewBrick"></div>
        <div class="searchMenu" id="searchMenuBrick">
        <div id="pagename">
            <form action="Server" method="post" id="searchForm">
                <select size="1" name="type">
                    <option value="1">${headers[6]}</option>
                    <option value="2">${headers[7]}</option>
                </select>
                <input type="hidden" name="action" value="searchItems"/>
                <input type="text" name="searchData" size="20" class="in" id="searchID" value="${headers[8]}" onclick="backspaceAll('searchID');" onfocusout="reWrite('searchID');">
                <input type="button" value="${headers[15]}" id="sendButton" class="button" onclick="send('searchForm');">
            </form>
        </div>
    </div>
    </div>
    <div style="position:absolute; margin-top: 0;" id="start"></div>
    <div class="whiteShadow"></div>
    <div class="header">
        <form action="Server" method="get" id="startPage">
            <img src="../IMG/home.png" width="40" height="40" onclick="sendOther('startPage');">
        </form>
        ${headers[11]}
    </div>
    <div class="footer">${headers[12]}</div>
    <a href="#start"><div class="up">${headers[16]}</div></a>
</body>
    <script src="../js/search.js"></script>
    <script src="../js/items.js"></script>
</html>