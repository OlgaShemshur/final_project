﻿<%@ page language="java" contentType="text/html" pageEncoding="utf-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.ArrayList"%>
<html>
<head>
    <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style2.css" rel="stylesheet">
    <title>Работа, вакансии, резюме</title>
</head>
<body>
    <div class="menuLine"></div>
    <div class="searchMenu" id="menuBrick"></div>
    <div class="content">
        <div id="mainLayer">
           <div class="result">
                <jsp:useBean id="ob" scope="page" class="servlet.pl.View" />
                <c:forEach var="item" items="${allData}" >
                  	<div>
                        <div class="titleName"><c:out value="${item.position}"/></div>
                        <div><b>з/п <c:out value="${item.salary}"/> <c:out value="${item.salaryCurrency}"/></b></div>
                        <div><c:out value="${item.requirements}"/></div>
                        <c:set var="employer" value="${ob.getEmployerByID(item)}"/>
                        <div>Контакты: <c:out value="${employer.email}"/> <c:out value="${employer.telephoneNumber}"/></div>
                        <div>Компания: <c:out value="${ob.getCompanyByID(employer).name}"/></div>
                        <div>Время добавлеия: <c:out value="${item.timeAdded}"/></div>
                        <form action="Server" method="post" id="moreForm">
                            <input type="hidden" name="action" value="moreInfo"/>
                            <button id="moreInfoButton" onclick="send('moreForm');">Подробнее...</button>
                        </form>
                    </div>
               	</c:forEach>
            </div>
        </div>
        <div class="menuLineBrick">
            <a href="../rus/registration.jsp"><input type="button" value="Регистрация" class="button" style="margin-left: -300px; margin-top: -15px;"></a>
            <a href="../rus/autorisation.jsp"><input type="button" value="Войти" class="button" style="margin-left: -125px; margin-top: -15px;"></a>
            <span>Язык/Language</span><img src="../IMG/rus.png" height="25" width="25" alt="rus">
            <select>
                <option>Русский</option>
                <option>English</option>
            </select>
        </div>
        <div class="searchMenu" id="searchMenuSkewBrick"></div>
        <div class="searchMenu" id="searchMenuBrick">
        <div id="pagename">
            <form action="Server" method="post" id="searchForm">
                <select size="1" name="type">
                    <option value="1">Вакансии</option>
                    <option value="2">Компании</option>
                </select>
                <input type="hidden" name="action" value="searchItems"/>
                <input type="text" name="searchData" size="20" class="in" id="searchID" value="Поиск..." onclick="backspaceAll('searchID');" onfocusout="reWrite('searchID');">
                <input type="button" value="Найти" id="sendButton" class="button" onclick="send('searchForm');">
            </form>
        </div>
    </div>
    </div>
    <div style="position:absolute; margin-top: 0;" id="start"></div>
    <div class="header">Найдите работу мечты</div>
    <div class="footer">2017 год</div>
    <a href="#start"><div class="up">Вверх</div></a>
</body>
    <script src="../js/search.js"></script>
</html>