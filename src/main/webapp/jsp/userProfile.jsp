﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head lang="ru">
    <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style2.css" rel="stylesheet">
    <meta charset="UTF-8">
    <title>${headers[0]}</title>
</head>
<body>
    <div class="tabloid"></div>
    <div class="under"></div>
    <div class="menuLine"></div>
    <div class="searchMenu" id="menuBrick"></div>
    <div class="content">
        <div id="mainLayer">
        <div id="mess">
            <span id="inner_text_message"></span>
        </div>
        <c:if test="${role=='user'}">
            <div class="rightBlock" style="height:360px;">
                <ul>
                    <li><c:out value="${user.surname}"/> <c:out value="${user.name}"/></li>
                    <li>${content[0]}<c:out value="${user.login}"/></li>
                    <li>${content[1]}<br><c:out value="${user.dateOfRegistration}"/></li>
                </ul>
            </div>
            <jsp:useBean id="ob" scope="page" class="servlet.presentationLayer.View" />
            <c:set var="employee" value="${ob.getEmployeeById(user.id)}"/>
            <div class="whileBlock">
                <ul>
                    <form action="Server" method="post" id="1">
                        <li>${content[2]}<input type="text" name="login" value="${user.login}"> <input type="button" value="${content[17]}" onclick="sendOther('1'); showMessage('${content[19]}');"><div></div></li>
                        <input type="hidden" name="action" value="update"/>
                        <input type="hidden" name="type" value="login"/>
                    </form>
                    <form action="Server" method="post" id="2">
                        <li>${content[3]}<input type="password" name="password" value="${user.password}"> <input type="button" value="${content[17]}" onclick="sendOther('2'); showMessage('${content[19]}');"><div></div></li>
                        <input type="hidden" name="action" value="update"/>
                        <input type="hidden" name="type" value="password"/>
                    </form>
                    <form action="Server" method="post" id="3">
                        <li>${content[4]}<input type="text" name="surname" value="${user.surname}"> <input type="button" value="${content[17]}"onclick="sendOther('3'); showMessage('${content[19]}');"><div></div></li>
                        <input type="hidden" name="action" value="update"/>
                        <input type="hidden" name="type" value="surname"/>
                    </form>
                    <form action="Server" method="post" id="4">
                        <li>${content[5]}<input type="text" name="name" value="${user.name}"> <input type="button" value="${content[17]}" onclick="sendOther('4'); showMessage('${content[19]}');"><div></div></li>
                        <input type="hidden" name="action" value="update"/>
                        <input type="hidden" name="type" value="name"/>
                    </form>
                    <form action="Server" method="post" id="5">
                        <li>${content[6]}
                            <select name="sex" style="margin-left:68px;">
                                <c:if test="${employee.sex == 'м'}">
                                    <option disabled selected>${content[7]}</option>
                                </c:if>
                                <c:if test="${employee.sex == 'ж'}">
                                    <option disabled selected>${content[8]}</option>
                                </c:if>
                                <c:if test="${employee.sex == 'н'}">
                                    <option disabled selected>${content[9]}</option>
                                </c:if>
                                <option value="м">${content[7]}</option>
                                <option value="ж">${content[8]}</option>
                                <option value="н">${content[9]}</option>
                            </select>
                            <input type="button" value="${content[17]}" onclick="sendOther('5'); showMessage('${content[19]}');">
                        </li>
                        <input type="hidden" name="action" value="update"/>
                        <input type="hidden" name="type" value="sex"/>
                    </form>
                    <form action="Server" method="post" id="6">
                        <li>${content[10]}<input type="text" name="email" value="${employee.email}"> <input type="button" value="${content[17]}" onclick="sendOther('6'); showMessage('${content[19]}');"><div></div></li>
                        <input type="hidden" name="action" value="update"/>
                        <input type="hidden" name="type" value="email"/>
                    </form>
                    <form action="Server" method="post" id="8">
                        <li>${content[11]}<input type="text" name="number" value="${employee.telephoneNumber}"> <input type="button" value="${content[17]}" onclick="sendOther('8'); showMessage('${content[19]}');"><div></div></li>
    `                   <input type="hidden" name="action" value="update"/>
                        <input type="hidden" name="type" value="number"/>
                    </form>
                    <form action="Server" method="post" id="9">
                        <li>${content[12]}<input type="date" value="${employee.dateOfBirthday}" name="birthday"> <input type="button" value="${content[17]}"  onclick="sendOther('9'); showMessage('${content[19]}');"><div></div></li>
                        <input type="hidden" name="action" value="update"/>
                        <input type="hidden" name="type" value="birthday"/>
                    </form>
                    <form action="Server" method="post" id="10">
                        <li><input type="button" value="${content[16]}"  onclick="sendOther('10'); showMessage('${content[19]}');"><div></div></li>
                        <input type="hidden" name="action" value="delete"/>
                        <input type="hidden" name="type" value="user"/>
                    </form>
                </ul>
            </div>
            <form action="Server" method="get" id="interviews">
                <div class="block2 light" style="width:200px; color:#ffffff; padding:10px; margin-top: 20px;" onclick="sendOther('interviews');">
                    <img src="IMG/interview.png" alt="${content[3]}" width="50px" height="50px">
                    <span  class="text" style="bottom: 20px;">${content[13]}</span>
                    <input type="hidden" name="page" value="allInterviews"/>
                </div>
            </form>
            <form action="Server" method="get" id="interviewResults">
                <div class="block2 light" style="width:200px; color:#ffffff; padding:10px; margin-top: 100px;" onclick="sendOther('interviewResults');">
                    <img src="IMG/pencil.png" alt="${content[4]}" width="50px" height="50px">
                    <span  class="text" style="bottom: 20px;">${content[14]}<br>${content[15]}</span>
                    <input type="hidden" name="page" value="allInterviewResults"/>
                </div>
            </form>
        </c:if>
        <c:if test="${empty role}">
            <div class="text" style="width: 100%; height:auto; overflow:hidden;">${headers[20]}</div>
        </c:if>
        </div>
        <div class="menuLineBrick">
        <c:if test="${empty role}">
            <form action="Server" method="get" id="registration">
                <input type="hidden" name="page" value="registration"/>
                <input type="button" value='${headers[9]}' class="button" id="but1" onclick="sendOther('registration');">
            </form>
                <form action="Server" method="get" id="authorization">
                <input type="hidden" name="page" value="authorization"/>
                <input type="button" value='${headers[10]}' class="button" id="but2" onclick="sendOther('authorization');">
            </form>
        </c:if>
        <c:if test="${role == 'user'}">
            <form action="Server" method="get" id="companies">
                <input value="${headers[18]}" type="submit">
                <input type="hidden" name="page" value="allCompanies"/>
            </form>
            <form action="Server" method="get" id="resume">
                <input value="${headers[19]}" type="submit">
                <input type="hidden" name="page" value="resume"/>
            </form>
        </c:if>
        <form action="Server" method="post" id="languageForm">
            <input type="hidden" name="action" value="language"/>
            <span>${headers[1]}</span>
            <img src="${headers[2]}" height="25" width="25" alt="${headers[3]}">
            <select onChange="sendOther('languageForm');" name="type">
                <option value="${headers[13]}">${headers[4]}</option>
                <option value="${headers[14]}">${headers[5]}</option>
            </select>
        </form>
    <c:if test="${role == 'user'}">
        <form action="Server" method="post" id="endSessionForm">
            <input type="hidden" name="action" value="endSession"/>
            <input type="button" id="exit" value="${headers[17]}" onclick="sendOther('endSessionForm');"/>
        </form>
    </c:if>
        </div>
        <div class="searchMenu" id="searchMenuSkewBrick"></div>
        <div class="searchMenu" id="searchMenuBrick">
        <div id="pagename">
            <form action="Server" method="post" id="searchForm">
                <select size="1" name="type">
                    <option value="1">${headers[6]}</option>
                    <option value="2">${headers[7]}</option>
                </select>
                <input type="hidden" name="action" value="searchItems"/>
                <input type="text" name="searchData" size="20" class="in" id="searchID" value="${headers[8]}" onclick="backspaceAll('searchID');" onfocusout="reWrite('searchID');">
                <input type="button" value="${headers[15]}" id="sendButton" class="button" onclick="send('searchForm');">
            </form>
        </div>
        </div>
    </div>
    <div class="header">
        <form action="Server" method="get" id="startPage">
            <img src="../IMG/home.png" width="40" height="40" onclick="sendOther('startPage');">
        </form>
        ${headers[11]}
    </div>
    <div class="footer">${headers[12]}</div>
</body>
    <script src="../js/search.js"></script>
    <script src="../js/validity.js"></script>
</html>