﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head lang="ru">
    <link rel="icon" href="../IMG/icon.ico" type="image/x-icon"/>
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style2.css" rel="stylesheet">
    <meta charset="UTF-8">
    <title>${headers[0]}</title>
</head>
<body>
    <div class="tabloid"></div>
    <div class="under"></div>
    <div class="menuLine"></div>
    <div class="searchMenu" id="menuBrick"></div>
    <div class="content">
        <div id="mainLayer">
        <div id="mess">
            <span id="inner_text_message"></span>
        </div>
        <c:if test="${role=='user'}">
            <div class="rightBlock" style="height:360px;">
                <ul>
                    <li><c:out value="${user.surname}"/> <c:out value="${user.name}"/></li>
                    <li>${content[0]}<c:out value="${user.login}"/></li>
                    <li>${content[1]}<br><c:out value="${user.dateOfRegistration}"/></li>
                </ul>
            </div>
            <jsp:useBean id="ob" scope="page" class="servlet.presentationLayer.View" />
            <c:set var="employee" value="${ob.getEmployeeById(user.id)}"/>
            <div class="whileBlock" style="height:auto; overflow:hidden;">
                <ul>
                    <form action="Server" method="post" id="1">
                        <li>
                        ${content[2]}
                        <select name="education">
                            <c:if test="${employee.education == 'не указано'}">
                                <option disabled selected>${content[9]}</option>
                            </c:if>
                            <c:if test="${employee.education == 'средее'}">
                                <option disabled selected>${content[10]}</option>
                            </c:if>
                            <c:if test="${employee.education == 'средее специальное'}">
                                <option disabled selected>${content[11]}</option>
                            </c:if>
                            <c:if test="${employee.education == 'среднее техническое'}">
                                <option disabled selected>${content[12]}</option>
                            </c:if>
                            <c:if test="${employee.education == 'высшее'}">
                                <option disabled selected>${content[13]}</option>
                            </c:if>
                            <option value="среднее">${content[10]}</option>
                            <option value="средее специальное">${content[11]}</option>
                            <option value="среднее техническое">${content[12]}</option>
                            <option value="высшее">${content[13]}</option>
                            <option value="не указано">${content[9]}</option>
                        </select>
                        <input type="button" value="${content[4]}" onclick="sendOther('1'); showMessage('${content[5]}');"><div></div></li>
                        <input type="hidden" name="action" value="update"/>
                        <input type="hidden" name="type" value="education"/>
                    </form>
                    <form action="Server" method="post" id="2">
                        <li>${content[3]}<input type="button" value="${content[4]}" style="margin-left:200px" onclick="sendOther('2'); showMessage('${content[5]}');">
                        <textarea style="margin-top: 10px;" rows ="19" cols="64" name="other">${employee.other}</textarea><div></div></li>
                        <input type="hidden" name="action" value="update"/>
                        <input type="hidden" name="type" value="other"/>
                    </form>
                </ul>
            </div>
            <form action="Server" method="get" id="interviews">
                <div class="block2 light" style="width:200px; color:#ffffff; padding:10px; margin-top: 20px;" onclick="sendOther('interviews');">
                    <img src="IMG/interview.png" alt="${content[6]}" width="50px" height="50px">
                    <span  class="text" style="bottom: 20px;">${content[6]}</span>
                    <input type="hidden" name="page" value="allInterviews"/>
                </div>
            </form>
            <form action="Server" method="get" id="interviewResults">
                <div class="block2 light" style="width:200px; color:#ffffff; padding:10px; margin-top: 100px;" onclick="sendOther('interviewResults');">
                    <img src="IMG/pencil.png" alt="${content[7]}" width="50px" height="50px">
                    <span  class="text" style="bottom: 20px;">${content[7]}<br>${content[8]}</span>
                    <input type="hidden" name="page" value="allInterviewResults"/>
                </div>
            </form>
        </c:if>
        <c:if test="${empty role}">
            <div class="text" style="width: 100%; height:auto; overflow:hidden;">${headers[20]}</div>
        </c:if>
        </div>
        <div class="menuLineBrick">
        <c:if test="${empty role}">
            <form action="Server" method="get" id="registration">
                <input type="hidden" name="page" value="registration"/>
                <input type="button" value='${headers[9]}' class="button" id="but1" onclick="sendOther('registration');">
            </form>
                <form action="Server" method="get" id="authorization">
                <input type="hidden" name="page" value="authorization"/>
                <input type="button" value='${headers[10]}' class="button" id="but2" onclick="sendOther('authorization');">
            </form>
        </c:if>
        <c:if test="${role == 'user'}">
            <form action="Server" method="get" id="companies">
                <input value="${headers[18]}" type="submit">
                <input type="hidden" name="page" value="allCompanies"/>
            </form>
        </c:if>
        <form action="Server" method="post" id="languageForm">
            <input type="hidden" name="action" value="language"/>
            <span>${headers[1]}</span>
            <img src="${headers[2]}" height="25" width="25" alt="${headers[3]}">
            <select onChange="sendOther('languageForm');" name="type">
                <option value="${headers[13]}">${headers[4]}</option>
                <option value="${headers[14]}">${headers[5]}</option>
            </select>
        </form>
    <c:if test="${role == 'user'}">
        <form action="Server" method="post" id="endSessionForm">
            <input type="hidden" name="action" value="endSession"/>
            <input type="button" id="exit" value="${headers[17]}" onclick="sendOther('endSessionForm');"/>
        </form>
    </c:if>
        </div>
        <div class="searchMenu" id="searchMenuSkewBrick"></div>
        <div class="searchMenu" id="searchMenuBrick">
        <div id="pagename">
            <form action="Server" method="post" id="searchForm">
                <select size="1" name="type">
                    <option value="1">${headers[6]}</option>
                    <option value="2">${headers[7]}</option>
                </select>
                <input type="hidden" name="action" value="searchItems"/>
                <input type="text" name="searchData" size="20" class="in" id="searchID" value="${headers[8]}" onclick="backspaceAll('searchID');" onfocusout="reWrite('searchID');">
                <input type="button" value="${headers[15]}" id="sendButton" class="button" onclick="send('searchForm');">
            </form>
        </div>
        </div>
    </div>
    <div class="header">
        <form action="Server" method="get" id="startPage">
            <img src="../IMG/home.png" width="40" height="40" onclick="sendOther('startPage');">
        </form>
        ${headers[11]}
    </div>
    <div class="footer">${headers[12]}</div>
</body>
    <script src="../js/search.js"></script>
    <script src="../js/validity.js"></script>
</html>